#!/usr/bin/perl

use strict;
use warnings;

use SDL2;
use SDLx::App;
use SDLx::Text;

my $app = SDLx::App->new(-height => 320,
			 -width  => 240,
			 -depth  => 16,
			 -title  => 'my-pilotpython');

my $message=SDLx::Text->new(font => '/usr/share/fonts/truetype/gentium-basic/GenBasB.ttf');
$message->write_to($app,"hello");
$app->update;

sleep(5);
