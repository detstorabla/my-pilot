#!/usr/bin/perl -w
use strict;
use Device::SerialPort qw( :PARAM :STAT 0.07);
use Switch;
use Math::Trig;

my $debug=1;


# My global variables
my $lat="";
my $lat_prev="";
my $lat_ns="";
my $lon="";
my $lon_prev="";
my $lon_ew="";
my $pos_fix="0";
my $fix_quality=0;
my $speed="0";
my $heading="0";
my $anchor_lat="5945.1400";
my $anchor_lon="01815.4911";

# Constants
# Radiuses
use constant NO_ACTION_RADIUS => 3.0;
use constant STEP_UP_ACTION_RADIUS => 5.0;
use constant FULL_ACTION_RADIUS => 10.0;

# Speed constants. Initial setting must depend on the speed of the drifting.
# Speed range 1
# 0.1-0.5 knots - Use lowest power (LOWPOWER) from start
# 0.5-1.3 knots - Use medium power(MEDIUMPOWER) from start for BOOST_TIME
# 1.3 - Apply power boost for BOOST_TIME to stop boat. 

use constant BOOST_TIME=>"2"; 
use constant LOWPOWER=>"1"; 
use constant MEDIUMPOWER=>"3"; 
use constant HIGHPOWER=>"6"; 


sub convert_pos{
    my $lat=shift;
    my $lon=shift;
    my @result;

#    print "DBG:lat=$lat\n";
#    print "DBG:lon=$lon\n";


    my @tmp;
    @tmp=split "",$lat;
    my $lathour=join "",$tmp[0],$tmp[1];
    shift @tmp;
    shift @tmp;
    my $latmin=join "",@tmp;

    @tmp=split "",$lon;
    my $lonhour=join "",$tmp[0],$tmp[1],$tmp[2];
    shift @tmp;
    shift @tmp;
    shift @tmp;
    my $lonmin=join "",@tmp;
    
    push @result,$lathour;
    push @result,$latmin;
    push @result,$lonhour;
    push @result,$lonmin;
    return @result;
}

sub calculate_initial_power_setting{
    my $distance=shift;
    my $speed=shift;
    
    
}




# Functions to be moved to it's own pm
sub calculate_distance_and_bearing{
    my $lat=shift;
    my $lon=shift;
    my $alat=shift;
    my $alon=shift;
    
    my @pos=&convert_pos($lat,$lon);
    my @apos=&convert_pos($alat,$alon);

 
    my $dy = (($apos[0]*60 + $apos[1]) - ($pos[0]*60 + $pos[1]))*1852 ;
    my $dx = (($apos[2]*60 + $apos[3]) - ($pos[2]*60 + $pos[3]))*1852 ;
#    print "DBG: Delta= ($dy,$dx) meter\n";
    my $d=sqrt($dy**2+$dx**2);
    my $h=atan($dy/$dx);
    # If $dy < 0 & $dx > 0, add 90 degrees 
    # If $dy < 0 & $dx < 0, add 180 degrees 
    # If $dy > 0 & $dx < 0, add 270 degrees 
    if (($dy < 0) & ($dx>0)) {
	$h = $h+90;
    }elsif (($dy < 0) & ($dx<0)) {
	$h = $h+180;
    }elsif (($dy > 0) & ($dx<0)) {
	$h = $h+270;
    };
   
    
    my @result;
    push @result,$d;
    push @result,$h;

    return @result;

   
}



sub decode_gpgll{
    my $line=pop;
    my @tmp=split ",",$line;
    $lat = $tmp[1];
    $lat_ns = $tmp[2];
    $lon = $tmp[3];
    $lon_ew = $tmp[4];
}

sub decode_gpgsa{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Satellite Status (GSA)\n";
}
sub decode_gpgga{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Essential Fix Data (GGA)\n";
    $fix_quality=$tmp[4];
    if (($tmp[4] eq "0") && ($pos_fix==1)) {
	$pos_fix = 0;
	print "# Position lost!\n";
    }
    if (($tmp[4] ne "0") && ($pos_fix==0)) {
	$pos_fix = 1;
	print "# Position aquired with fix quality = $fix_quality\n";
	if ($debug) {
	    $anchor_lat=$tmp[2];
	}

    } 
}

sub decode_gpgsv{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Satellites in view (GSV)\n";
}
sub decode_gpvtg{
    my $line=pop;
    my @tmp=split ",",$line;
    print "# DBG Velocity Made Good (VTG)\n";
}
sub decode_pgrme{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Garmin prop sentence, hor/ver error in meters (PGRME)\n";
}
sub decode_pgrmz{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Garmin prop sentence, altidute in feet (PGRMZ)\n";
}
sub decode_gprte{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Waypoint info \n";
}
sub decode_gprmb{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG is sent whenever a route or a goto is active (GPRMB)\n";
}
sub decode_gpbod{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG Origin to Destination bearing, calculated at the origin waypoint,(GPBOD) \n";
}
sub decode_gprmc{
    my $line=pop;
    my @tmp=split ",",$line;
#    print "# DBG NMEA has its own version of essential gps pvt (GPRMC) \n";
    my $status=$tmp[2];
#    if ($status eq "A") {
	$speed = $tmp[7];
	$heading=$tmp[8];
	$lat=$tmp[3];
	$lat_ns=$tmp[4];
	$lon=$tmp[5];
	$lon_ew=$tmp[6];
 #   }

}

my $PORT="/dev/ttyUSB0";
my $ob= Device::SerialPort->new($PORT)|| die "Can't open $PORT : $!\n";
$ob->baudrate(4800);
$ob->write_settings;
open (SERIAL, "+>$PORT");
print "Waiting for NMEA data...\n";
while (my $line = <SERIAL>){
    my @l = split ",",$line;
    $l[0] =~ s/\$//;
    switch ($l[0]) {
	case "GPGLL" {&decode_gpgll($line);}
	case "GPGSA" {&decode_gpgsa($line); }
	case "GPGGA" {&decode_gpgga($line); }
	case "GPGSV" {&decode_gpgsv($line); }
	case "GPVTG" {&decode_gpvtg($line); }
	case "PGRME" {&decode_pgrme($line); }
	case "PGRMZ" {&decode_pgrmz($line); }
	case "GPRTE" {&decode_gprte($line); }
	case "GPRMB" {&decode_gprmb($line); }
	case "GPBOD" {&decode_gpbod($line); }
	case "GPRMC" {&decode_gprmc($line); }
	else {
	    print "# NOTE!!!! CHECK:$line\n";
	}
    }
    # Display current location, velocity and direction.
    if ($pos_fix) {
	if (($lat_prev ne $lat) || ($lon_prev ne $lon)) { 
	    my @anchor=&calculate_distance_and_bearing($lat,$lon,$anchor_lat,$anchor_lon);	    
#	    print "# Lat =$lat $lat_ns ,Long=$lon $lon_ew, Speed=$speed knots, Heading=$heading degrees $anchor[0] m $anchor[1] degrees\n";
	    $lat_prev=$lat;
	    $lon_prev=$lon;
	    
	    # let's emulate control
	    if ($anchor[0] < "3.0") {
		print "###############################################################################\n";
		print "# We are happy where we are (within 3 m)\n";
		print "# Turn engine against reference point - no power\n";
	    } else {
		print "###############################################################################\n";
		print "# Time to start adjusting (distance=$anchor[0] m)\n";
		print "# Turn engine against reference point (direction=$anchor[1] degrees)\n";
		print "# Set power setting to TBD (dep on distance,speed,mass)\n";
		print "# Start Engine\n";
	    }




	}
    } 




}
